# Commands for updating the image:
# (1) docker build --pull -t registry.gitlab.com/sosy-lab/test-comp/archives-2020/user:latest - < Dockerfile.user
# (2) Create a Gitlab token under https://gitlab.com/profile/personal_access_tokens
#     - Name: "dockerSVCOMParchives" or so
#     - ExpiresAt: e.g., 2020-05-31
#     - Scope: API is sufficient
#     IMPORTANT: Remember new secret token!
# (3) docker login registry.gitlab.com
#     - Username=dbeyer
#     - Password: above generated secret token
# (4) docker push registry.gitlab.com/sosy-lab/test-comp/archives-2020/user:latest

FROM ubuntu:18.04
ENV DEBIAN_FRONTEND=noninteractive
RUN dpkg --add-architecture i386
RUN apt-get update && apt-get install -y \
       openjdk-11-jdk-headless \
       gcc-multilib \
       libgomp1 \
       make \
       clang \
       clang-3.9 \
       clang-6.0 \
       clang-7 \
       expect \
       libc6-dev \
       lldb-3.9 \
       llvm-6.0 \
       mono-devel \
       gcc-5-multilib \
       gcc-7-multilib \
       python-minimal \
       python \
       python-lxml \
       libc6-dev-i386 \
       linux-libc-dev:i386 \
       openjdk-8-jdk-headless \
       python-sklearn \
       python-pandas \
       python-pycparser \
       python3-setuptools \
       unzip
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Requirements from SV-COMP'20 and Test-Comp'19:
#
# (Fetch latest version from the Ansible configuration for the competition machines:
#  https://gitlab.com/sosy-lab/admin/sysadmin/ansible/blob/master/roles/benchmarking/tasks/main.yml)
#
#      - openjdk-11-jdk-headless
#      - gcc-multilib # cpp, development headers
#      - libgomp1 # for Z3
#      - make # for fshellw2t
#      - clang # SV-COMP'19 AProVE
#      - clang-3.9 # SV-COMP'20 Dartagnan
#      - clang-6.0 # Test-Comp'19, SV-COMP'20 VeriAbs
#      - clang-7 # SV-COMP'20 VeriFuzz
#      - expect # SV-COMP'20 PredatorHP
#      - libc6-dev # SV-COMP'20 Map2Check
#      - lldb-3.9 # SV-COMP'20 Dartagnan
#      - llvm-6.0 # Test-Comp'19, SV-COMP'20 VeriAbs
#      - mono-devel # SV-COMP'19 AProVE, SMACK
#      - gcc-5-multilib # SV-COMP'19 PredatorHP
#      - gcc-7-multilib # SV-COMP'20 PredatorHP
#      - python-minimal # SV-COMP'20 Map2Check
#      - python # SV-COMP'20 PredatorHP, Symbiotic
#      - python-lxml # SV-COMP'20 Symbiotic
#      - libc6-dev-i386 # SV-COMP'20 CBMC, VeriAbs, VeriFuzz
#      - linux-libc-dev:i386 # SV-COMP'20 CBMC
#      - openjdk-8-jdk-headless # SV-COMP'20 Ultimate
#      - python-sklearn # SV-COMP'20 VeriFuzz
#      - python-pandas # SV-COMP'20 VeriFuzz
#      - python-pycparser # SV-COMP'19 CSeq
#      - python3-setuptools # SecC
#      - unzip # SV-COMP'19 JBMC
